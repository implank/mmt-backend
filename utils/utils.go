package utils

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

//申请的信息
var appID = 20230517001680409
var password = "n6HlMJWUMzaojzu_H3Qc"

//百度翻译api接口
var Url = "http://api.fanyi.baidu.com/api/trans/vip/translate"

type TranslateModel struct {
	Q     string
	From  string
	To    string
	Appid int
	Salt  int
	Sign  string
}

func NewTranslateModeler(q, from, to string) TranslateModel {
	tran := TranslateModel{
		Q:    q,
		From: from,
		To:   to,
	}
	tran.Appid = appID
	tran.Salt = time.Now().Second()
	content := strconv.Itoa(appID) + q + strconv.Itoa(tran.Salt) + password
	sign := SumString(content) //计算sign值
	tran.Sign = sign
	return tran
}
func (tran TranslateModel) ToValues() url.Values {
	values := url.Values{
		"q":     {tran.Q},
		"from":  {tran.From},
		"to":    {tran.To},
		"appid": {strconv.Itoa(tran.Appid)},
		"salt":  {strconv.Itoa(tran.Salt)},
		"sign":  {tran.Sign},
	}
	return values
}

func SumString(content string) string {
	md5Ctx := md5.New()
	md5Ctx.Write([]byte(content))
	bys := md5Ctx.Sum(nil)
	value := hex.EncodeToString(bys)
	return value
}
func BaiduTranslate(query string) (translated string, err error) {
	tran := NewTranslateModeler(query, "auto", "en")
	values := tran.ToValues()
	resp, err := http.PostForm(Url, values)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	var data map[string]interface{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	translated = data["trans_result"].([]interface{})[0].(map[string]interface{})["dst"].(string)
	return translated, nil
}
