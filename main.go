package main

import (
	"fmt"
	"net/http"
	"os"

	v1 "example.com/api/v1"
	"example.com/docs"
	"example.com/global"
	"example.com/middleware"
	"example.com/model"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func InitRouter(r *gin.Engine) {
	r.Use(middleware.Cors()) // 跨域
	// r.Use(middleware.Log())  // 日志

	docs.SwaggerInfo.Title = "backend doc"
	docs.SwaggerInfo.Version = "v1"
	docs.SwaggerInfo.BasePath = "/"
	docs.SwaggerInfo.Schemes = []string{"http", "https"}

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))

	r.GET("/api/test", testGin)
	r.GET("/api/music", v1.GetMusic)
	r.GET("/api/query_music", v1.QueryMusic)
	r.GET("/api/music/all", v1.GetAllMusic)
	r.POST("/api/music/diy", v1.MtmtMusic)
	r.POST("/api/music/sander/gen", v1.SanderMusic)
	r.GET("/api/music/sander/all_music", v1.SanderAllMusic)

	musicFolder := fmt.Sprintf("../%s/exp", viper.GetString("mmt-dir"))
	_, err := os.Stat(musicFolder)
	if !os.IsNotExist(err) {
		r.StaticFS("/api/music-folder", http.Dir(musicFolder))
	}
	musicFolder2 := fmt.Sprintf("../%s/results", viper.GetString("mmt-dir"))
	_, err = os.Stat(musicFolder2)
	if !os.IsNotExist(err) {
		r.StaticFS("/api/music-folder2", http.Dir(musicFolder2))
	}
	SanderMusicFolder := "../text-to-music/output"
	r.StaticFS("/api/music/sander/files", http.Dir(SanderMusicFolder))
}

func InitViper() {
	viper.SetConfigFile("./config.yml")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("fatal error config file: %s", err))
	}
}

func InitGorm() {
	ip := viper.GetString("db.ip")
	port := viper.GetString("db.port")
	user := viper.GetString("db.user")
	password := viper.GetString("db.password")
	schema := viper.GetString("db.schema")
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		user, password, ip, port, schema)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(fmt.Errorf("数据库出问题啦: %s ", err))
	}
	db.AutoMigrate(
		&model.Task{},
	)
	global.DB = db
}

func testGin(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "pong",
		"success": true,
	})
}

func main() {
	r := gin.Default()

	InitViper()
	InitGorm()
	InitRouter(r)

	if err := r.Run(":8080"); err != nil {
		panic(err)
	}
}
