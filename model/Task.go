package model

import "gorm.io/gorm"

type Task struct {
	gorm.Model
	Hashcode  string `json:"hashcode"`
	Status    string `json:"status"`
	ModelType string `json:"model_type"`
}
