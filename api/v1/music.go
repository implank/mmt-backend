package v1

import (
	"crypto/md5"
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math/rand"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"example.com/global"
	"example.com/model"
	"example.com/utils"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"gorm.io/gorm"
)

// GetMusic doc
// @Description 生成音乐 decorated!!! 已废弃
// @Tags Music
// @Param seq_len query string false "音乐长度，默认为1024"
// @Success 200 {string} string "{"message": "请等待生成成功","hashcode": "hashcode"}"
// @Router /api/music [get]
func GetMusic(c *gin.Context) {
	seqLen := c.DefaultQuery("seq_len", "1024")

	md5Code := md5.Sum([]byte(time.Now().String()))
	hashcode := fmt.Sprintf("%s_%x",
		time.Now().Format("2006-01-02_15:04:05"), md5Code)
	cmd := exec.Command("python", "mmt/generate.py",
		"-d", "sod",
		"-o", "exp/sod/ape",
		"-g", "0",
		"--n_samples", "1",
		"--seq_len", seqLen,
		"-on", hashcode,
	)
	cmd.Dir = fmt.Sprintf("../%s", viper.GetString("mmt-dir"))
	runTask(cmd, hashcode)
	c.JSON(200, gin.H{
		"message":  "请等待生成成功",
		"hashcode": hashcode,
	})
}

// python mmt/generate.py -d sod -o exp/sod/ape -g 0 --n_samples 1 --seq_len 2048
// python mmt/mygen.py -d sod -o exp/sod/ape -g 0 -on 123
func QueryMusic(c *gin.Context) {
	hashcode := c.DefaultQuery("hashcode", "none")
	if hashcode == "none" {
		c.JSON(400, gin.H{
			"message": "缺少hashcode参数",
		})
		return
	}
	println("query music hashcode:" + hashcode)
	hashcode = strings.ReplaceAll(hashcode, " ", "")
	var task model.Task
	if res := global.DB.Where("hashcode = ?", hashcode).First(&task); errors.Is(res.Error, gorm.ErrRecordNotFound) {
		c.JSON(404, gin.H{
			"message": "未找到该任务",
		})
		return
	} else {
		if res.Error != nil {
			panic(fmt.Errorf("query task fail: %s ", res.Error))
		}
	}
	c.JSON(200, gin.H{
		"message": "查询成功",
		"task":    task,
		"status":  task.Status,
	})
}

// GetAllMusic doc
// @Description 获取所有音乐生成任务的状态
// @Tags Music
// @Success 200 {string} string "{"message": "查询成功","tasks": []model.Task}"
// @Router /api/music/all [get]
func GetAllMusic(c *gin.Context) {
	var tasks []model.Task
	if res := global.DB.Raw("SELECT * FROM tasks Where model_type = ?", "mmt").Scan(&tasks); res.Error != nil {
		panic(fmt.Errorf("query task fail: %s ", res.Error))
	}
	c.JSON(200, gin.H{
		"message": "查询成功",
		"tasks":   tasks,
	})
}

var genreDict map[string]string = map[string]string{
	"sod":                    "sod",
	"genre-bluegrass":        "genre-bluegrass",
	"genre-blues":            "genre-blues",
	"genre-christian-gospel": "genre-christian-gospel",
	"genre-country":          "genre-country",
	"genre-disco":            "genre-disco",
	"genre-folk":             "genre-folk",
	"genre-hip-hop-rap":      "genre-hip-hop-rap",
	"genre-jazz":             "genre-jazz",
	"genre-pop":              "genre-pop",
	"genre-punk":             "genre-punk",
	"genre-rnb-soul":         "genre-rnb-soul",
	"genre-rock":             "genre-rock",
	"0":                      "sod",
	"1":                      "genre-bluegrass",
	"2":                      "genre-blues",
	"3":                      "genre-christian-gospel",
	"4":                      "genre-country",
	"5":                      "genre-disco",
	"6":                      "genre-folk",
	"7":                      "genre-hip-hop-rap",
	"8":                      "genre-jazz",
	"9":                      "genre-pop",
	"10":                     "genre-punk",
	"11":                     "genre-rnb-soul",
	"12":                     "genre-rock",
}

// DiyMusic doc
// @Description 自定义生成，支持指定乐器
// @Tags Music
// @Param instruments formData string false "用逗号分隔的英文乐器列表，如piano,violin，默认为piano"
// @Param genre formData string false "音乐风格，默认为sod，请输入具体的音乐风格或对应代号0-12"
// @Param beat formData string false "midi文件中保留的前缀节拍，不填默认为不截取"
// @Param control_inst formData string false "控制乐器，默认为n（生成音乐包含所提供的乐器但不限于）"
// @Param midifile formData file false "midi文件"
// @Failure 400 {string} string "{"message": "不存在的乐器"}"
// @Success 200 {string} string "{"message": "请等待生成成功","hashcode": "hashcode"}"
// @Router /api/music/diy [post]
func MtmtMusic(c *gin.Context) {
	hashcode := fmt.Sprintf("%s", time.Now().Format("2006-01-02_15:04:05"))

	var instruments []string
	instStr := c.DefaultPostForm("instruments", "piano")
	genre := c.DefaultPostForm("genre", "sod")
	controlInst := c.DefaultPostForm("control_inst", "n")
	beatStr := c.DefaultPostForm("beat", "64")

	if g, ok := genreDict[genre]; !ok {
		c.JSON(400, gin.H{
			"message": "不存在的音乐风格",
		})
		return
	} else {
		genre = g
	}

	instruments = strings.Split(instStr, ",")
	// read json file
	encodingFile, err := os.Open("../" + viper.GetString("mmt-dir") + "/mmt/encoding.json")
	if err != nil {
		panic(fmt.Errorf("open encoding file fail: %s ", err))
	}
	defer encodingFile.Close()
	encoding := make(map[string]interface{})
	if err := json.NewDecoder(encodingFile).Decode(&encoding); err != nil {
		panic(fmt.Errorf("decode encoding file fail: %s ", err))
	}
	instrument_program_map := encoding["instrument_program_map"].(map[string]interface{})
	// check instruments
	for _, instrument := range instruments {
		if _, ok := instrument_program_map[instrument]; !ok {
			c.JSON(400, gin.H{
				"message": "不存在的乐器",
			})
			return
		}
	}

	beat, err := strconv.Atoi(beatStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": "beat参数错误",
		})
	}

	// new csv file
	mmtDir := "../" + viper.GetString("mmt-dir")
	workDir := fmt.Sprintf("%s/results/%s/", mmtDir, hashcode)
	os.MkdirAll(workDir, 0755)

	midiFile, err := c.FormFile("midifile")
	upload := false
	if err == nil {
		c.SaveUploadedFile(midiFile, workDir+"uploadmidi.mid")
		// convert midi file to csv
		workDirPy := fmt.Sprintf("results/%s/", hashcode)
		cmd := exec.Command("python", "mmt/convert.py",
			"-n", "uploadmidi",
			"-i", workDirPy,
			"-o", workDirPy,
		)
		cmd.Dir = mmtDir
		if err := cmd.Run(); err != nil {
			_ = fmt.Errorf("upload midi file fail: %s \n", err)
			goto tag1
		}
		cmd = exec.Command("python", "mmt/myextract.py",
			"-n", "uploadmidi",
			"-i", workDirPy,
			"-o", workDirPy,
			"-b", strconv.Itoa(beat),
		)
		cmd.Dir = mmtDir
		if err := cmd.Run(); err != nil {
			_ = fmt.Errorf("upload midi file fail: %s \n", err)
			goto tag1
		}
		upload = true
		os.Rename(workDir+"uploadmidi.csv", workDir+"notes.csv")
	tag1:
	} else {
		_ = fmt.Errorf("upload midi file fail: %s \n", err)
	}

	namesFile, err := os.Create(workDir + "names.txt")
	if err != nil {
		panic(fmt.Errorf("create names file fail: %s ", err))
	}
	namesFile.Write([]byte(string("notes")))
	defer namesFile.Close()

	var notes [][]string
	var csvFile *os.File
	if !upload {
		notes = append(notes, []string{"beat", "position", "pitch", "duration", "program"})
		csvFile, err = os.Create(workDir + "notes.csv")
	} else {
		csvFile, err = os.OpenFile(workDir+"notes.csv", os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	}
	if err != nil {
		panic(fmt.Errorf("create csv file fail: %s ", err))
	}
	defer csvFile.Close()
	csvWriter := csv.NewWriter(csvFile)
	for i, instrument := range instruments {
		notes = append(notes, []string{"-1", "0", "0", "0", strconv.Itoa(int(instrument_program_map[instrument].(float64)))})
		if i == 0 {
			notes = append(notes, []string{"-1", "0", "0", "0", strconv.Itoa(int(instrument_program_map[instrument].(float64)))})
		}
	}
	csvWriter.WriteAll(notes)
	csvWriter.Flush()
	// run python script
	// python mmt/mygen.py -d SOD -o exp/genre-rock/ape -g 0 -on 123 -i data/sod/processed/notes
	cmd := exec.Command("python", "mmt/mygen.py",
		"-d", "sod",
		"-o", fmt.Sprintf("exp/%s/ape", genre),
		"-i", "data/sod/processed/notes",
		"-g", "0",
		"-on", hashcode,
	)
	if controlInst != "n" {
		cmd.Args = append(cmd.Args, "-ci")
	}
	cmd.Dir = fmt.Sprintf("../%s", viper.GetString("mmt-dir"))
	runTask(cmd, hashcode)
	c.JSON(200, gin.H{
		"message":  "请等待生成成功",
		"upload":   upload,
		"hashcode": hashcode,
	})
}

func runTask(cmd *exec.Cmd, hashcode string) {
	fmt.Println(cmd)
	if res := global.DB.Create(&model.Task{Hashcode: hashcode, Status: "running", ModelType: "mmt"}); res.Error != nil {
		panic(fmt.Errorf("insert task fail: %s ", res.Error))
	}
	go func() {
		stdout, err := cmd.StdoutPipe()
		if err != nil {
			fmt.Println("Error creating StdoutPipe for Cmd", err)
			return
		}
		if err := cmd.Start(); err != nil {
			fmt.Println("Error starting Cmd", err)
			return
		}
		if _, err := io.Copy(os.Stdout, stdout); err != nil {
			fmt.Println("Error copying output from Cmd", err)
			return
		}
		err = cmd.Wait()
		if err != nil {
			if res := global.DB.Exec("UPDATE tasks SET status = ? WHERE hashcode = ?",
				"error", hashcode); res.Error != nil {
				panic(fmt.Errorf("update task:%s fail: %s ", hashcode, res.Error))
			}
			fmt.Printf("Task:%s finished with error: %s\n", hashcode, err)
		} else {
			if res := global.DB.Exec("UPDATE tasks SET status = ? WHERE hashcode = ?",
				"done", hashcode); res.Error != nil {
				panic(fmt.Errorf("update task:%s fail: %s ", hashcode, res.Error))
			}
			fmt.Printf("Task:%s finished successfully\n", hashcode)
		}
	}()
}

// SanderMusic godoc
// @Description sander text2music
// @Tags Music
// @Param prompt formData string false "prompt"
// @Param max_length formData string false "max_length，长度"
// @Success 200 {string} string "{"message": "请等待生成成功","hashcode": "hashcode"}"
// @Router /api/music/sander/gen [post]
func SanderMusic(c *gin.Context) {
	prompt := c.DefaultPostForm("prompt", "This is a traditional Irish dance music.")
	num_tunes := "1"
	max_length := c.DefaultPostForm("max_length", "2048")
	seed := strconv.Itoa(rand.Intn(19260817))

	tsPrompt, err := utils.BaiduTranslate(prompt)
	if err != nil {
		fmt.Printf("translate prompt fail: %s ", err)
		c.JSON(401, gin.H{
			"message": "translate prompt fail",
		})
	}
	prompt = tsPrompt

	hashcode := fmt.Sprintf("%s", time.Now().Format("2006-01-02_15:04:05"))
	inputFile, err := os.Create("../text-to-music/input/" + hashcode + ".txt")
	if err != nil {
		panic(fmt.Errorf("create input file fail: %s ", err))
	}
	defer inputFile.Close()
	inputFile.Write([]byte(prompt + "\n"))
	padding := `Note Length-1/8
Meter-6/8
Key-D`
	inputFile.Write([]byte(padding))
	cmd := exec.Command("python", "myinfer.py",
		"-seed", seed,
		"-max_length", max_length,
		"-num_tunes", num_tunes,
		"-input_path", "./input/"+hashcode+".txt",
		"-output_path", "./output/"+hashcode+".abc",
	)
	cmd.Dir = "../text-to-music"
	fmt.Println(cmd)
	if res := global.DB.Create(&model.Task{Hashcode: hashcode,
		Status:    "running",
		ModelType: "text-to-music",
	}); res.Error != nil {
		panic(fmt.Errorf("insert task fail: %s ", res.Error))
	}
	go func() {
		stdout, err := cmd.StdoutPipe()
		if err != nil {
			fmt.Println("Error creating StdoutPipe for Cmd", err)
			return
		}
		if err := cmd.Start(); err != nil {
			fmt.Println("Error starting Cmd", err)
			return
		}
		if _, err := io.Copy(os.Stdout, stdout); err != nil {
			fmt.Println("Error copying output from Cmd", err)
			return
		}
		err = cmd.Wait()
		if err != nil {
			if res := global.DB.Exec("UPDATE tasks SET status = ? WHERE hashcode = ?",
				"error", hashcode); res.Error != nil {
				panic(fmt.Errorf("update task:%s fail: %s ", hashcode, res.Error))
			}
			fmt.Printf("Task:%s finished with error: %s\n", hashcode, err)
		} else {
			if res := global.DB.Exec("UPDATE tasks SET status = ? WHERE hashcode = ?",
				"done", hashcode); res.Error != nil {
				panic(fmt.Errorf("update task:%s fail: %s ", hashcode, res.Error))
			}
			fmt.Printf("Task:%s finished successfully\n", hashcode)
		}
	}()
	c.JSON(200, gin.H{
		"message":  "请等待生成成功",
		"hashcode": hashcode,
	})
}

// SanderAllMusic
// @Description 查询text2music的所有任务进度
// @Tags Music
// @Success 200 {string} string "{"message": "查询成功","tasks": []model.Task}"
// @Router /api/music/sander/all_music [get]
func SanderAllMusic(c *gin.Context) {
	var tasks []model.Task
	if res := global.DB.Raw("SELECT * FROM tasks Where model_type = ?", "text-to-music").Scan(&tasks); res.Error != nil {
		panic(fmt.Errorf("query task fail: %s ", res.Error))
	}
	c.JSON(200, gin.H{
		"message": "查询成功",
		"tasks":   tasks,
	})
}
